var pack = require('../helpers/pack'),
  colors = require('colors'),
  fs = require('fs'),
  request = require('request'),
  config = require('../helpers/config'),
  prompt = require('../helpers/prompt'),
  uuid = require('node-uuid');

var readmePath = process.cwd() + '/README.md';

function publish() {
  if (!fs.existsSync(readmePath)) {
    console.log("No README.md exists!".red);
    process.exit(1);
  } else {
    var readme = fs.readFileSync(readmePath).toString();
    var id = extractId(readme);

    if (id) {
      update(id, readme);
    } else {
      create(readme);
    }
  }
};

function create(md) {
  var id = uuid.v4(),
      newReadme = "<!-- " + id + " -->\n" + md,
      title = extractTitle(md),
      tags;

  prompt('Enter some tags (comma separated)'.green, function(tagStr) {
    tags = tagStr.split(',');
    finishCreate();
  });

  function finishCreate() {
    pack(process.cwd(), function(buff) {
      request.post({
        url: config.get('serverUrl') + '/exercises',
        json: { 
          exercise: {
            id: id, 
            readme: newReadme,
            title: title,
            data: buff.toString('base64'), 
            tags: tags,
            author: config.get('name')
          }
        }
      }, function(err, res, body) {
        if (err || res.statusCode != 200) {
          console.log('Error'.red)
        } else {
          console.log('Success!'.green);
          fs.writeFileSync(readmePath, newReadme);
        }
      });
    });
  }
}

function update(id, md) {
  var title = extractTitle(md);

  pack(process.cwd(), function(buff) {
    request.put({
      url: config.get('serverUrl') + '/exercises/' + id,
      json: { 
        exercise: {
          readme: md,
          title: title,
          data: buff.toString('base64'), 
          author: config.get('name')
        }
      }
    }, function(err, res, body) {
      if (err || res.statusCode != 200) {
        console.log('Error'.red)
      } else {
        console.log('Success!'.green);
        fs.writeFileSync(readmePath, newReadme);
      }
    });
  });
}

function extractId(md) {
  // find the id in the first comment of the markdown
  var match = md.match(/<\!--\s*(\S+)\s*-->/);

  return match && match[1];
}

function extractTitle(md) {
  var match = md.match(/^#{1,3}\s*(.+)/);

  return match && match[1];
}

module.exports = publish;