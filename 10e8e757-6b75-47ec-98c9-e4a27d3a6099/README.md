<!-- 10e8e757-6b75-47ec-98c9-e4a27d3a6099 -->
# WEX (WDI Exercises)

## Installation

You must have node and npm installed.

```
  git clone git@github.com:generalassembly-wdi-materials/wex.git
  cd wlm
  npm link
```

You may also have to configure your environment to indicate where your globally installed node_modules are located.

`echo 'export NODE_PATH=/path/to/node/modules'  >> ~/.bash_profile`

Mine is at `/usr/local/lib/node_modules`

## Usage

1. `wex publish`
  * packages up a folder with a README and sends to WDI Exercise App
  
2. `wex get [id]`
  * downloads and unzips an exercise
