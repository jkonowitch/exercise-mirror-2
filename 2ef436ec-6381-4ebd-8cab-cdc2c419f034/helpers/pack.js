var stream = new require("stream").Writable(),
    file = new Array(),
    fstream = require('fstream'),
    tar = require('tar'),
    zlib = require('zlib');

var blacklistRegex = /^(\.git|\.DS_Store|node_modules)/;

stream._write = function (chunk, enc, next) {
  file.push(chunk);
  next();
};

function pack(folder, cb) {
  stream.end = function (data) {
    cb(Buffer.concat(file));
  };

  fstream.Reader({ 
    path: folder, 
    type: 'Directory',
    filter: function () {
      return !this.basename.match(blacklistRegex);
    }
  })
  .pipe(tar.Pack())/* Convert the directory to a .tar file */
  .pipe(zlib.Gzip())/* Compress the .tar file */
  .pipe(stream)
};

module.exports = pack;