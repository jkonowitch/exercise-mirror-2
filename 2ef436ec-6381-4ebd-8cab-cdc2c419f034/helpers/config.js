var path = require("path");
var config = require("nconf");

config.file(path.resolve(process.env.HOME, ".wex", "config"));

module.exports = config;