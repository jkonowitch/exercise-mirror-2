function prompt(str, cb) {
  console.log(str);

  process.stdin.on('data', function(chunk) {
    process.stdin.pause();

    cb(chunk.toString().replace(/\n/, ''));
  });
}

module.exports = prompt;