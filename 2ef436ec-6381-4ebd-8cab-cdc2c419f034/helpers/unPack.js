var tar = require('tar'),
    zlib = require('zlib'),
    streamifier = require('streamifier');

function unPack(package, cb) {
  var buffer = new Buffer(package, 'base64');

  streamifier.createReadStream(buffer)
    .pipe(zlib.Gunzip())
    .pipe(tar.Extract({path: process.cwd()}))
    .on('end', cb);
};

module.exports = unPack;