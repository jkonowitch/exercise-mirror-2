var program = require("commander"),
  config = require('./helpers/config'),
  prompt = require('./helpers/prompt');

// set user's name on first use

if (!config.get('name')) {
  console.log('here');
  prompt('First time using wex. Please enter your name.', function(name) {
    config.set('name', name);
    config.save();
    run();
  });
} else {
  run();
};

function run() {
  program
      .version('0.0.1')

  program
    .command('pub')
    .description('publish lesson')
    .action(function(env){
      require('./lib/publish')();
    });

  program
    .command('get [id]')
    .description('download a lesson')
    .action(function(id) {
      require('./lib/get')(id);
    });

  program.parse(process.argv);
}